<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<style type="text/css">
			#question{
				color: red;
			}
		</style>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Array Entfernen</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"GET\" action=\"delarray.php\">\n";
	echo "			System:<select name=\"sys\">\n";
	if(isset($_GET["sys"])){
		echo "					<option value=\"des\"";
		if($_GET["sys"] == "des"){
				echo " selected";
				$devTable = "";
		}
		echo ">design</option>\n";
		echo "				<option value=\"test\"";
		if($_GET["sys"] == "test"){
				echo " selected";
				$devTable = "DEV";
		}
		echo ">dmp testing</option>\n";
		$sys = $_GET["sys"];
	} else {
		echo "				<option value=\"des\" selected>design</option>\n";
		echo "				<option value=\"test\">dmp testing</option>\n";
		$sys = "des";
		$devTable = "";
	}
	echo "			</select>\n";
	echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
	echo "			<input type=\"submit\">\n";
	echo "		</form><br>\n";
	echo "		<h4>Array: " . $_GET["men"] . "</h4>\n";
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
}
?>
		<br><br>
<?php
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"POST\" action=\"delarray.php\">\n";
	echo "			<h4 id=\"question\">Soll das Men&uuml; wirklich gel&ouml;scht werden?</h4>\n";
	echo "			<b>Betroffene Items</b><br><br>\n";
	$deQ = sprintf("SELECT EntryText FROM translates%s t join MenuItem%s it on t.MenuItem=it.ID AND (t.lang='de' OR t.lang='all') where it.ConnectedMenu='%s';", $devTable, $devTable, $_GET["men"]);
	$deR = mysqli_query($DBcon, $deQ);
	if(mysqli_num_rows($deR) > 0){
		echo "			";
		while($deTXT = mysqli_fetch_array($deR)){
			echo $deTXT["EntryText"] . ",";
		}
		echo "<br><br><br>\n";
	} else {
		echo "			<i>keine</i><br><br><br>\n";
	}
	echo "			<input type=\"submit\" value=\"Ja\">\n";
	echo "			<a href=\"index.php?sys=" . $sys . "\"><input type=\"button\" value=\"Nein\"></a>&emsp;\n";
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"index.php?sys=" . $sys . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	$PM = 0;
	# have to move 'PointsMenu'?
	$chkPMq = sprintf("SELECT count(*) AS cnt FROM Page%s WHERE PointsMenu = '%s';", $devTable, $_POST["men"]);
	$chkPMr = mysqli_query($DBcon, $chkPMq);
	if ($chkPMr) {
		$chkPM = mysqli_fetch_array($chkPMr);
		if ($chkPM["cnt"] > 0) {
			$PM = 1;
		}
	} else {
		echo mysqli_error($DBcon) . "<br>\n";
		mysqli_close($DBcon);
		die("Can not check PointsMenu");
	}
	# if to have move 'PointsMenu'...
	if($PM){
		# for move pages away that point to the menu collect another 'best coice' to replace the pointer
		$rPMq = sprintf("SELECT PointsMenu, cnt FROM (SELECT count(PointsMenu) AS cnt, PointsMenu FROM Page%s WHERE NOT PointsMenu = '%s' GROUP BY PointsMenu) grp HAVING max(cnt);", $devTable, $_POST["men"]);
		$rPMr = mysqli_query($DBcon, $rPMq);
		if($rPMr){
			$mvPMa = mysqli_fetch_array($rPMr);
			if ($mvPMa["cnt"] != 0) {
				$mvPM = $mvPMa["PointsMenu"];
			} else {
				echo mysqli_error($DBcon) . "<br>\n";
				mysqli_close($DBcon);
				die("No Move target for moving PointsMenu found!");
			}
		} else {
			echo mysqli_error($DBcon) . "<br>\n";
			mysqli_close($DBcon);
			die("Can not fetch move target for moving PointsMenu away.");
		}
		# now move
		$mvPMq = sprintf("UPDATE Page%s SET PointsMenu='%s', HighlightsItem=0 WHERE PointsMenu='%s';", $devTable, $mvPM, $_POST["men"]);
		$mvPMr = mysqli_query($DBcon, $mvPMq);
		if($mvPMr){
			echo "Moved PointsMenu " . mysqli_affected_rows($DBcon) . " Times.<br>\n";
		} else {
			echo mysqli_error($DBcon) . "<br>\n";
			mysqli_rollback($DBcon);
			mysqli_close($DBcon);
			die("Error on moving PointsMenu.");
		}
	}
	# set superior to 0 for dep. menus
	echo "Move dep. Menus away...";
	$sK0q = sprintf("UPDATE Menu%s SET SuperiorKey = '0' WHERE SuperiorKey = '%s';", $devTable, $_POST["men"]);
	$sK0r = mysqli_query($DBcon, $sK0q);
	if($sK0r){
		echo "Updated " . mysqli_affected_rows($DBcon) . "<br>\n";
	} else {
		echo "Failed : " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
		mysqli_close($DBcon);
		die("Error on move dep menus away.");
	}
	# set Submenu Value of other Menu Items to 0
	echo "Resetting Submenu pointer on foreign menu items...";
	$sS0q = sprintf("UPDATE MenuItem%s SET Submenu = '0' WHERE Submenu = '%s';", $devTable, $_POST["men"]);
	$sS0r = mysqli_query($DBcon, $sS0q);
	if($sS0r){
		echo "Updated " . mysqli_affected_rows($DBcon) . "<br>\n";
	} else{
		echo "Failed " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
		mysqli_close($DBcon);
		die();
	}
	# begin delete... for all menu items of this menu, remove translations
	$dTq = sprintf("DELETE FROM translates%s WHERE MenuItem IN (SELECT ID AS MenuItem FROM MenuItem%s WHERE ConnectedMenu = '%s');", $devTable, $devTable, $_POST["men"]);
	$dTr = mysqli_query($DBcon, $dTq);
	if($dTr){
		echo "Deleted translations " . mysqli_affected_rows($DBcon) . "<br>\n";
	} else {
		echo "Failed " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
		mysqli_close($DBcon);
		die();	
	}

	$dIq = sprintf("DELETE FROM MenuItem%s WHERE ConnectedMenu = '%s';", $devTable, $_POST["men"]);
	$dIr = mysqli_query($DBcon, $dIq);
	if($dIr){
		echo "Deleted Items " . mysqli_affected_rows($DBcon) . "<br>\n";
	} else {
		echo "Failed " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
		mysqli_close($DBcon);
		die();
	}

	$dMq = sprintf("DELETE FROM Menu%s WHERE MenuKey = '%s';", $devTable, $_POST["men"]);
	if(mysqli_query($DBcon, $dMq)){
		echo "		Deleted menu!<br>\n";
		mysqli_commit($DBcon);
	} else {
		echo "		Fehler: " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
}
mysqli_close($DBcon);
?>
	</body>
</html>
