<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Array &Uuml;bersicht</h3>
		<form method="GET" action="index.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
echo "		<a href=\"/excl/dmp/index.php?sys=" . $sys . "\">Hauptmen&uuml;</a>\n";
echo "		<a href=\"add/addform.php?sys=" . $sys . "\"><input type=\"button\" value=\"Neues leeres Men&uuml; array\"></a>\n";
?>
		<br><br>
		<table>
			<thead>
				<tr>
					<th>Men&uuml; Schl&uuml;ssel</th>
					<th>&Uuml;bergeordnet</th>
					<th>Optionen</th>
				</tr>
			</thead>
			<tbody>
<?php
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$q = sprintf("SELECT * FROM Menu%s;", $devTable);
$r = mysqli_query($DBcon, $q);
while($row = mysqli_fetch_array($r)){
	echo "				<tr>\n";
	echo "					<td><a href=\"../items/index.php?sys=" . $sys . "&men=" . $row["MenuKey"] . "\">" . $row["MenuKey"] . "</a></td>\n";
	echo "					<td>" . $row["SuperiorKey"] . "</td>\n";
	echo "					<td>\n";
	echo "						<a href=\"superior/modform.php?sys=" . $sys . "&men=" . $row["MenuKey"] . "\"><input type=\"button\" value=\"&Uuml;bergeodnetes Men&uuml; &auml;ndern\"></a>\n";
	echo "						<a href=\"delarray.php?sys=" . $sys . "&men=" . $row["MenuKey"] . "\"><input type=\"button\" value=\"L&ouml;schen\"></a>\n";
	echo "					</td>\n";
	echo "				</tr>\n";
}
mysqli_close($DBcon);
?>
			</tbody>
		</table>	
	</body>
</html>
