<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Item hinzuf&uuml;gen</h3>
		<form method="GET" action="addform.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"submit\">\n";
echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
echo "		</form><br>\n";
echo "		<br><br>\n";
echo "		<form method=\"POST\" action=\"addaction.php\">\n";
$mxIDq = sprintf("SELECT max(ID) + 1 AS nextID FROM MenuItem%s;", $devTable);
include "../../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$mxIDr = mysqli_query($DBcon, $mxIDq);
if(mysqli_num_rows($mxIDr) > 0){
	$mxID = mysqli_fetch_array($mxIDr);
	if(!is_null($mxID["nextID"])){
		$nextID = $mxID["nextID"];
	} else {
		$nextID = 0;
	}
} else {
	$nextID = 0;
}
echo "			Men&uuml; ID:<input type=\"text\" name=\"id\" value=\"" . $nextID . "\" required>\n";
?>
			&emsp;
			Rank: <input type="number" name="rank" value="0">
			<br>
			Erste Sprache:
			<label><input type="radio" name="lang" value="all" checked>Pseudo Sprache &quot;all&quot;</label>
			<label><input type="radio" name="lang" value="de">Deutsch</label>
			<br>
			Deutscher Eintrag:<input type="text" name="entry" size="50"><br>
			Submenu Verweis:<select name="submenu">
				<option value="0" selected>Kein Verweis</option>
<?php
$menuQ = sprintf("SELECT MenuKey FROM Menu%s;", $devTable);
$menuR = mysqli_query($DBcon, $menuQ);
while ($menus = mysqli_fetch_array($menuR)) {
	echo "				<option value=\"" . $menus["MenuKey"] . "\">" . $menus["MenuKey"] . "</option>\n";
}
mysqli_close($DBcon);
?>
			</select><br>
			<label for="url-field">URL:</label>
			<textarea id="url-field" name="url" rows="4" cols="100" placeholder="./relative/site.php?lang=x (max. 1000 Zeichen)"></textarea><br>
			<input type="submit">
<?php
echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
echo "			<a href=\"../index.php?sys=" . $sys . "&men=" . $_GET["men"] . "\"><input type=\"button\" name=\"btn_cncl\" value=\"Abbrechen\"></a>\n";
?>	
		</form>
	</body>
</html>
