<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<style type="text/css">
			#question{
				color: red;
			}
		</style>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Item Entfernen</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"GET\" action=\"delitem.php\">\n";
	echo "			System:<select name=\"sys\">\n";
	if(isset($_GET["sys"])){
		echo "					<option value=\"des\"";
		if($_GET["sys"] == "des"){
				echo " selected";
				$devTable = "";
		}
		echo ">design</option>\n";
		echo "				<option value=\"test\"";
		if($_GET["sys"] == "test"){
				echo " selected";
				$devTable = "DEV";
		}
		echo ">dmp testing</option>\n";
		$sys = $_GET["sys"];
	} else {
		echo "				<option value=\"des\" selected>design</option>\n";
		echo "				<option value=\"test\">dmp testing</option>\n";
		$sys = "des";
		$devTable = "";
	}
	echo "			</select>\n";
	echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"id\" value=\"". $_GET["id"] . "\">\n";
	echo "			<input type=\"submit\">\n";
	echo "		</form><br>\n";
	echo "		<h4>Array: " . $_GET["men"] . "</h4>\n";
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
}
?>
		<br><br>
<?php
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"POST\" action=\"delitem.php\">\n";
	echo "			<h4 id=\"question\">Soll der Eintrag wirklich gel&ouml;scht werden?</h4>\n";
	echo "			<b>Deutscher Eintrag</b>&emsp;\n";
	$deQ = sprintf("SELECT EntryText FROM translates%s t join MenuItem%s it on t.MenuItem=it.ID AND (t.lang='de' OR t.lang='all') where it.ID=%d;", $devTable, $devTable, $_GET["id"]);
	$deR = mysqli_query($DBcon, $deQ);
	if($deR){
		$deTXT = mysqli_fetch_array($deR);
		echo $deTXT["EntryText"] . "<br><br><br>\n";
	}
	echo "			<input type=\"submit\" value=\"Ja\">\n";
	echo "			<a href=\"index.php?sys=" . $sys . "\"><input type=\"button\" value=\"Nein\"></a>&emsp;\n";
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"index.php?sys=" . $sys . "&men=" . $_GET["men"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	
	# begin delete... for all menu items of this menu, remove translations
	$dTq = sprintf("DELETE FROM translates%s WHERE MenuItem = %d;", $devTable, $_POST["id"]);
	$dTr = mysqli_query($DBcon, $dTq);
	if($dTr){
		echo "Deleted translations " . mysqli_affected_rows($DBcon) . "<br>\n";
	} else {
		echo "Failed " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
		mysqli_close($DBcon);
		die();	
	}

	$dIq = sprintf("DELETE FROM MenuItem%s WHERE ID = %d;", $devTable, $_POST["id"]);
	$dIr = mysqli_query($DBcon, $dIq);
	if($dIr){
		echo "Deleted Item " . mysqli_affected_rows($DBcon) . "<br>\n";
		mysqli_commit($DBcon);
	} else {
		echo "Failed " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
}
mysqli_close($DBcon);
?>
	</body>
</html>
