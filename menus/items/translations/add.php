<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Item &Uuml;bersetzung hinzuf&uuml;gen</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"GET\" action=\"add.php\">\n";
	echo "			System:<select name=\"sys\">\n";
	if(isset($_GET["sys"])){
		echo "					<option value=\"des\"";
		if($_GET["sys"] == "des"){
				echo " selected";
				$devTable = "";
		}
		echo ">design</option>\n";
		echo "				<option value=\"test\"";
		if($_GET["sys"] == "test"){
				echo " selected";
				$devTable = "DEV";
		}
		echo ">dmp testing</option>\n";
		$sys = $_GET["sys"];
	} else {
		echo "				<option value=\"des\" selected>design</option>\n";
		echo "				<option value=\"test\">dmp testing</option>\n";
		$sys = "des";
		$devTable = "";
	}
	echo "			</select>\n";
	echo "			<input type=\"hidden\" name=\"forkey\" value=\"" . $_GET["forkey"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">\n";
	echo "			<input type=\"submit\">\n";
	echo "		</form><br>\n";
	echo "		<a href=\"index.php?sys=" . $sys . "&men=" . $_GET["forkey"] . "&id=" . $_GET["id"] . "\">Zur&uuml;ck</a><br>\n";
	echo "		<h4>Array: " . $_GET["forkey"] . "</h4>\n";
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
}
?>
		<br><br>
<?php
include "../../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"POST\" action=\"add.php\">\n";
	echo "			Deutscher Men&uuml; Wert: ";
	$deQ = sprintf("SELECT EntryText FROM translates%s WHERE MenuItem=%d AND (lang='de' OR lang='all');", $devTable, $_GET["id"]);
	$deR = mysqli_query($DBcon, $deQ);
	$deTXT = mysqli_fetch_array($deR);
	echo $deTXT["EntryText"] . "<br>\n";
	echo "			&Uuml;bersetzung: <input type=\"text\" name=\"trans\">\n";
	echo "			Sprache:\n";
	$langsQ = "SELECT * FROM Language WHERE NOT abbreviation = 'de';";
	$langsR = mysqli_query($DBcon, $langsQ);
	echo "			<select name=\"lang\">\n";
	while($langs = mysqli_fetch_array($langsR)){
		echo "				<option value=\"" . $langs["abbreviation"] . "\">" . $langs["DisplayValue"] . "</option>\n";
	}
	echo "			</select>\n";
	echo "			<input type=\"submit\">\n";
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"index.php?sys=" . $sys . "&men=" . $_GET["forkey"] . "&id=" . $_GET["id"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	$iQ = sprintf("INSERT INTO translates%s(MenuItem, lang, EntryText) VALUES (%d, '%s', '%s');", $devTable, $_POST["id"], $_POST["lang"], $_POST["trans"]);
	if(mysqli_query($DBcon, $iQ)){
		echo "		Erfolgreich!<br>\n";
		mysqli_commit($DBcon);
	} else {
		echo "		Fehler: " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
}
mysqli_close($DBcon);
?>
	</body>
</html>
