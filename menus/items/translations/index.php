<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<script type="text/javascript">
			function DevTable(){
				return <?php echo "\"" . $_GET["sys"] . "\"";?>;
			}
			function sysparam(){
				return <?php echo "\"" . "sys=" . $_GET["sys"] . "&men=" . $_GET["men"] . "\""; ?>;
			}
			function chTXT(lang){
				document.getElementById("btn_" + lang).disabled = false;
			}
			function UpdateClick(lang, id){
				entry = document.getElementById("txt_" + lang).value;
				obj = { "entry":entry, "id":id, "lang":lang, "sys":DevTable() };
				sendobj = JSON.stringify(obj);
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("index.php?" + sysparam() + "&id=" + id);
       					}
       				}
				}
				xmlhttp.open("POST", "UpdateTrans.php", true);
       			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
       			xmlhttp.send("x=" + encodeURIComponent(sendobj));
			}
			function DelClick(lang, id){
				if(confirm("Soll übersetzung wirklich gelöscht werden?") == false){return;}
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("index.php?" + sysparam() + "&id=" + id);
       					}
       				}
				}
				xmlhttp.open("GET", "DelTrans.php?id=" + id + "&lang=" + lang + "&sys=" + DevTable(), true);
                xmlhttp.send();
			}
		</script>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Item &Uuml;bersetzungen</h3>
		<form method="GET" action="index.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
echo "			<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
echo "		<a href=\"/excl/dmp/index.php?sys=" . $sys . "\">Hauptmen&uuml;</a><br>\n";
echo "		<a href=\"../../arrays/index.php?sys=" . $sys . "\">Array Men&uuml;</a><br>\n";
echo "		<a href=\"../index.php?sys=" . $sys . "&men=" . $_GET["men"] . "\">Item Auflistung f&uuml;r Men&uuml; Key &quot;" . $_GET["men"] . "&quot;</a><br>\n";
echo "		<a href=\"add.php?sys=" . $sys . "&id=" . $_GET["id"] . "&forkey=" . $_GET["men"] . "\"><input type=\"button\" value=\"&Uuml;bersetzung hinzuf&uuml;gen\"></a>\n";
echo "		<h4>Array: " . $_GET["men"] . "</h4>\n";
?>
		<br><br>
		<table>
			<thead>
				<tr>
					<th>Sprache</th>
					<th>Eintags Text</th>
					<th>Optionen</th>
				</tr>
			</thead>
			<tbody>
<?php
include "../../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$q = sprintf("SELECT DisplayValue, EntryText, lang FROM Language join translates%s on abbreviation=lang where MenuItem=%d;", $devTable, $_GET["id"]);
$r = mysqli_query($DBcon, $q);
while($row = mysqli_fetch_array($r)){
	echo "				<tr>\n";
	echo "					<td>" . $row["DisplayValue"] . "</td>\n";
	echo "					<td><input type=\"text\" id=\"txt_" . $row["lang"] . "\" value=\"" . $row["EntryText"] . "\" onchange=\"chTXT('" . $row["lang"] . "')\"></td>\n";
	echo "					<td>\n";
	echo "						<input type=\"button\" id=\"btn_" . $row["lang"] . "\" onclick=\"UpdateClick('" . $row["lang"] . "', " . $_GET["id"] . ")\" value=\"Text &Auml;ndern\" disabled>\n";
	echo "						<input type=\"button\" onclick=\"DelClick('" . $row["lang"] . "', " . $_GET["id"] . ")\" value=\"L&ouml;schen\"";
	if(mysqli_num_rows($r) < 2){
		echo " disabled";
	}
	echo ">\n";
	echo "					</td>\n";
	echo "				</tr>\n";
}
mysqli_close($DBcon);
?>
			</tbody>
		</table>	
	</body>
</html>
