<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Item Verweis auf Submen&uuml; &auml;ndern</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"GET\" action=\"updateSubmenu.php\">\n";
	echo "			System:<select name=\"sys\">\n";
	if(isset($_GET["sys"])){
		echo "					<option value=\"des\"";
		if($_GET["sys"] == "des"){
				echo " selected";
				$devTable = "";
		}
		echo ">design</option>\n";
		echo "				<option value=\"test\"";
		if($_GET["sys"] == "test"){
				echo " selected";
				$devTable = "DEV";
		}
		echo ">dmp testing</option>\n";
		$sys = $_GET["sys"];
	} else {
		echo "				<option value=\"des\" selected>design</option>\n";
		echo "				<option value=\"test\">dmp testing</option>\n";
		$sys = "des";
		$devTable = "";
	}
	echo "			</select>\n";
	echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">\n";
	echo "			<input type=\"submit\">\n";
	echo "		</form><br>\n";
	echo "		<a href=\"index.php?sys=" . $sys . "&men=" . $_GET["men"] . "\">Zur&uuml;ck</a><br>\n";
	echo "		<h4>Array: " . $_GET["men"] . " - ID: " . $_GET["id"] . "</h4>\n";
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
}
?>
		<br><br>
<?php
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
if($_SERVER["REQUEST_METHOD"] == "GET"){
	$itemQ = sprintf("SELECT Submenu, ConnectedMenu FROM MenuItem%s WHERE ID=%d;", $devTable, $_GET["id"]);
	$itemR = mysqli_query($DBcon, $itemQ);
	$item = mysqli_fetch_array($itemR);
	echo "		<form method=\"POST\" action=\"updateSubmenu.php\">\n";
	echo "			Submenu Verweis:<select name=\"submenu\">\n";
	echo "				<option value=\"0\"";
	if($item["Submenu"] == "0"){ echo " selected";}
	echo ">Kein Verweis</option>\n";
	$menuQ = sprintf("SELECT MenuKey FROM Menu%s WHERE NOT MenuKey = '%s';", $devTable, $item["ConnectedMenu"]);
	$menuR = mysqli_query($DBcon, $menuQ);
	while ($menus = mysqli_fetch_array($menuR)) {
		echo "				<option value=\"" . $menus["MenuKey"] . "\"";
		if($item["Submenu"] == $menus["MenuKey"]){ echo " selected";}
		echo ">" . $menus["MenuKey"] . "</option>\n";
	}
	echo "			</select><br>\n";
	echo "			<input type=\"submit\">\n";
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"index.php?sys=" . $sys . "&men=" . $_GET["men"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	$iQ = sprintf("UPDATE MenuItem%s SET Submenu = '%s' WHERE ID=%d;", $devTable, $_POST["submenu"], $_POST["id"]);
	if(mysqli_query($DBcon, $iQ)){
		echo "		Erfolgreich!<br>\n";
		mysqli_commit($DBcon);
	} else {
		echo "		Fehler: " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
}
mysqli_close($DBcon);
?>
	</body>
</html>
