<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<script type="text/javascript">
			function DevTable(){
				return <?php echo "\"" . $_GET["sys"] . "\"";?>;
			}
			function sysparam(){
				return <?php echo "\"" . "sys=" . $_GET["sys"] . "&men=" . $_GET["men"] . "\""; ?>;
			}
			function chTXT(id){
				document.getElementById("btn_" + id).disabled = false;
			}
			function chURL(id){
				entry = document.getElementById("txt_" + id).value;
				obj = { "entry":entry, "id":id, "sys":DevTable() };
				sendobj = JSON.stringify(obj);
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("index.php?" + sysparam());
       					}
       				}
				}
				xmlhttp.open("POST", "webloc-update.php", true);
       			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
       			xmlhttp.send("x=" + sendobj);
			}
			function chRank(id){
				document.getElementById("btnRank_" + id).disabled = false;
			}
			function UpdateRank(id){
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("index.php?" + sysparam());
       					}
       				}
				}
				rank = document.getElementById("numRank_" + id).value;
				xmlhttp.open("GET", "UpdateRank.php?id=" + id + "&sys=" + DevTable() + "&rank=" + rank, true);
                xmlhttp.send();
			}
		</script>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Menu Items &Uuml;bersicht</h3>
		<form method="GET" action="index.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"hidden\" name=\"men\" value=\"" . $_GET["men"] . "\">\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
echo "		<a href=\"/excl/dmp/index.php?sys=" . $sys . "\">Hauptmen&uuml;</a><br>\n";
echo "		<a href=\"../arrays/index.php?sys=" . $sys . "\">Array Men&uuml;</a><br>\n";
echo "		<a href=\"add/addform.php?sys=" . $sys . "&men=" . $_GET["men"] . "\"><input type=\"button\" value=\"Neuen Men&uuml; Entrag\"></a>\n";
echo "		<h4>Array: " . $_GET["men"] . "</h4>\n";
?>
		<br><br>
		<table>
			<thead>
				<tr>
					<th>ID</th>
					<th>Rank</th>
					<th>Deutscher Eintrag</th>
					<th>Verf&uuml;gbar in Sprachen</th>
					<th>URL</th>
					<th>Men&uuml; Verweis</th>
					<th>Optionen</th>
				</tr>
			</thead>
			<tbody>
<?php
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$q = sprintf("SELECT * FROM MenuItem%s join translates%s on MenuItem=ID where ConnectedMenu = '%s' and (lang = 'de' OR lang='all') ORDER BY Rank;", $devTable, $devTable, $_GET["men"]);
$r = mysqli_query($DBcon, $q);
while($row = mysqli_fetch_array($r)){
	$langQ = sprintf("SELECT DisplayValue FROM Language join translates%s on abbreviation=lang where MenuItem=%d;", $devTable, $row["ID"]);
	$langR = mysqli_query($DBcon, $langQ);
	echo "				<tr>\n";
	echo "					<td>" . $row["ID"] . "</td>\n";
	echo "					<td><input type=\"number\" id=\"numRank_" . $row["ID"] . "\" value=\"" . $row["Rank"] . "\" onchange=\"chRank(" . $row["ID"] . ")\">\n";
	echo "					<td>" . $row["EntryText"] . "</td>\n";
	echo "					<td><ul>\n";
	while($langs = mysqli_fetch_array($langR)){
		echo "						<li>" . $langs["DisplayValue"] . "</li>\n";
	}
	echo "					</ul></td>\n";
	echo "					<td><input type=\"text\" id=\"txt_" . $row["ID"] . "\" onchange=\"chTXT(" . $row["ID"] . ")\" value=\"" . $row["URL"] . "\"></td>\n";
	echo "					<td>" . $row["Submenu"] . "</td>\n";
	echo "					<td>\n";
	echo "						<a href=\"translations/index.php?sys=" . $sys . "&men=" . $_GET["men"] . "&id=" . $row["ID"] . "\"><input type=\"button\" value=\"Anzeige Texte\"></a>\n";
	echo "						<input type=\"button\" id=\"btn_" . $row["ID"] . "\" onclick=\"chURL(" . $row["ID"] . ")\" value=\"URL &Auml;ndern\" disabled></a>\n";
	echo "						<input type=\"button\" id=\"btnRank_" . $row["ID"] . "\" onclick=\"UpdateRank(" . $row["ID"] . ")\" value=\"Rank &Auml;ndern\" disabled></a>\n";
	echo "						<a href=\"updateSubmenu.php?sys=" . $sys . "&men=" . $_GET["men"] . "&id=" . $row["ID"] . "\"><input type=\"button\" value=\"Submenu &auml;ndern\"></a>\n";
	echo "						<a href=\"delitem.php?sys=" . $sys . "&men=" . $_GET["men"] . "&id=" . $row["ID"] . "\"><input type=\"button\" value=\"L&ouml;schen\"></a>\n";
	echo "					</td>\n";
	echo "				</tr>\n";
}
mysqli_close($DBcon);
?>
			</tbody>
		</table>	
	</body>
</html>
