<?php
header('Content-Type: text/javascript');
?>
function sysparam(){
	return <?php echo "\"" . "sys=" . $_GET["sys"] . "&pid=" . $_GET["pid"] . "\""; ?>;
}
function DelTXT(aname, lang) {
	if(confirm("Soll der Absatz wirklich gelöscht werden?") == false){return;}
	xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText != "0"){
					alert(this.responseText);
				} else {
					RemPrevent();
					window.location.assign("modP.php?" + sysparam() + "&aname=" + aname);
				}
			}
	}
	xmlhttp.open("GET", "para/DelPara.php?aname=" + encodeURI(aname) + "&lang=" + lang + "&" + sysparam(), true);
    xmlhttp.send();

}