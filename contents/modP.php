<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<?php
		if($_SERVER["REQUEST_METHOD"] == "GET"){
			echo "<script type=\"text/javascript\" src=\"delPara.js.php?sys=" . $_GET["sys"] . "&pid=" . $_GET["pid"] . "\"></script>\n"; # fixes error when called py POST instead of GET
			echo "<script type=\"text/javascript\" src=\"TransPara.js.php?sys=" . $_GET["sys"] . "&pid=" . $_GET["pid"] . "\"></script>\n"; # fixes error when called py POST instead of GET
		}
		?>
		<script type="text/javascript">
			function AreaHasChanged() {
				window.addEventListener("beforeunload", PreventLeave);
				document.getElementById('area').removeEventListener("change", AreaHasChanged);
				document.getElementById('area_F').addEventListener("submit", RemPrevent);
			}
			function RemPrevent() {
				document.getElementById('area_F').removeEventListener("submit", RemPrevent);
				window.removeEventListener("beforeunload", PreventLeave);
			}
			function PreventLeave(e){
					e.preventDefault();
					e.returnValue = '';
			}
		</script>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Absatz Area bearbeiten</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	if(isset($_GET["sys"])){
		if($_GET["sys"] == "des"){
				$devTable = "";
		}
		if($_GET["sys"] == "test"){
				$devTable = "DEV";
		}
		$sys = $_GET["sys"];
	} else {
		$sys = "des";
		$devTable = "";
	}
	echo "		<a href=\"detailContent.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "\">Zur&uuml;ck</a><br>\n";
	$pid = $_GET["pid"];
	$aname = urldecode($_GET["aname"]);
	if(isset($_GET["lang"])){
		$slang = $_GET["lang"];
	} else {
		$slang = "";
	}
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
	$pid = $_POST["pid"];
	$aname = $_POST["aname"];
	$slang = $_POST["lang"];
	$sys = $_POST["sys"];
}
?>
		<br><br>
<?php
include "../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$langsQ = "SELECT * FROM Language WHERE abbreviation!='all';";
$langsR = mysqli_query($DBcon, $langsQ);
echo "		<form method=\"GET\" action=\"modP.php\">\n";
echo "			Sprache:<select name=\"lang\" id=\"lang\">\n";
$langs = mysqli_fetch_all($langsR);
foreach ($langs as list($lang_abbr, $lang_display)) {
	echo "				<option value=\"" . $lang_abbr . "\"";
	if($lang_abbr == $slang){ echo " selected";}
	echo ">" . $lang_display . "</option>\n";
}
echo "			</select>\n";
echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
echo "			<input type=\"hidden\" name=\"pid\" value=\"" . $pid . "\">\n";
echo "			<input type=\"hidden\" name=\"aname\" value=\"" . $aname . "\">\n";
echo "			<input type=\"submit\" value=\"Sprache laden\">\n";
echo "		</form>\n";
$pageQ = sprintf("SELECT Alias FROM Page%s WHERE ID = %d;", $devTable, $pid);
$pageR = mysqli_query($DBcon, $pageQ);
$page = mysqli_fetch_array($pageR);
echo "		<h4>Seite: " . $page["Alias"] . "</h4>\n";
echo "		<h4>Absatz: " . $aname . "</h4>\n";
if($_SERVER["REQUEST_METHOD"] == "GET"){
	if(isset($_GET["lang"])){
		$cQ = sprintf("SELECT TXT_Body FROM displaylang%s WHERE Area='%s' AND lang='%s' AND PageID = %d;", $devTable, $aname, $_GET["lang"], $pid);
		$cR = mysqli_query($DBcon, $cQ);
		$cont = mysqli_fetch_array($cR);
		$transQ = sprintf("SELECT DISTINCT l.* FROM displaylang%s dl JOIN Language l ON l.abbreviation=dl.lang WHERE dl.Area='%s' AND NOT l.abbreviation IN ('%s', 'all') AND dl.PageID = %d;", $devTable, $aname, $_GET["lang"], $pid);
		$transR = mysqli_query($DBcon, $transQ);
		if(mysqli_num_rows($transR) > 0){
			echo "		<div>\n";
			echo "			<span>&Uuml;bersetzen von:</span>";
			echo "<select id=\"translate\">\n";
			foreach ($langs as list($lang_abbr, $lang_display)) {
				if ($lang_abbr != $slang) {
					echo "				<option value=\"" . $lang_abbr . "\">" . $lang_display . "</option>\n";
				}
			}
			echo "			</select>\n";
			echo "			<button id=\"btnGO\" onclick=\"DoTranslate('" . $aname . "')\">Go</button>\n";
			echo "		</div>\n";
		}
	}
	echo "		<form method=\"POST\" action=\"modP.php\" id=\"area_F\">\n";
	if(isset($_GET["lang"])){
		echo "			<textarea name=\"body\" rows=\"40\" cols=\"200\" onchange=\"AreaHasChanged()\" id=\"area\">\n";
		if(mysqli_num_rows($cR) > 0){
			echo $cont["TXT_Body"];
		}
		echo "</textarea><br>\n";
	}
	if(isset($_GET["lang"])){
		echo "			<input type=\"submit\" value=\"Daten &auml;ndern\">\n";
		if(mysqli_num_rows($cR) > 0){ echo "			<input type=\"button\" value=\"L&ouml;schen\" onclick=\"DelTXT('" . $aname . "', '" . $_GET["lang"] . "')\">\n"; }
		echo "			<input type=\"hidden\" name=\"lang\" value=\"" . $_GET["lang"] . "\">\n";
		echo "			<input type=\"hidden\" name=\"update\" value=\"" . mysqli_num_rows($cR) . "\">\n";
	}
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"detailContent.php?sys=" . $sys . "&pid=" . $pid . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"pid\" value=\"" . $pid . "\">\n";
	echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
	echo "			<input type=\"hidden\" name=\"aname\" value=\"" . $aname . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	if($_POST["update"]){
		$iQ = sprintf("UPDATE displaylang%s SET TXT_Body = ? WHERE Area = '%s' AND lang = '%s' AND PageID = %d;", $devTable, $_POST["aname"], $_POST["lang"], $pid);
	} else {
		$iQ = sprintf("INSERT INTO displaylang%s(TXT_Body, lang, Area, PageID) VALUES (?, '%s', '%s', %d);", $devTable, $_POST["lang"], $_POST["aname"], $pid);
	}
	$prep = mysqli_prepare($DBcon, $iQ);
	if ($prep){
		if(mysqli_stmt_bind_param($prep, "s", $_POST["body"])){
			if(mysqli_stmt_execute($prep)){
				echo "		Erfolgreich!<br>\n";
				mysqli_commit($DBcon);
			} else {
				echo "		Fehler beim Ausführen: " . mysqli_error($DBcon) . "<br>\n";
				mysqli_rollback($DBcon);
			}
		}
		else{
			echo "		Fehler beim binden: " . mysqli_error($DBcon) . "<br>\n";
		}
		mysqli_stmt_close($prep);
	} else {
		echo "		Fehler beim Vorbereiten: " . mysqli_error($DBcon) . "<br>\n";
	}
}
mysqli_close($DBcon);
?>
	</body>
</html>
