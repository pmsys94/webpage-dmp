<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<style type="text/css">
			div#topDiv{
				width: 90%;
				float: left;
			}
			div#tDIV{
				width: 90%;
				float: left;
			}
			div#topDiv div#lDIV{
				float: left;
			}
			.linking{
				margin-right: 10px;
			}
			.cEdit{
				margin-left: 10px;
			}
		</style>
		<script type="text/javascript">
			function FilterTagType() {
<?php
	if(isset($_GET["sys"])){
		echo "				sys=\"" . $_GET["sys"] . "\";\n";
	} else{
		echo "				sys=\"\";\n";
	}
	if(isset($_GET["pid"])){
		echo "				pid=\"" . $_GET["pid"] . "\";\n";
	} else {
		echo "				pid=\"\";\n";
	}
?>
				tagtype=document.getElementById("TagOption").value;
				if (sys.length == 0) {return;}
				if (pid.length == 0) {return;}
				if (tagtype == "*") {
					tag="";
				} else {
					tag = "&tag=" + tagtype;
				}
				window.location.assign("detailContent.php?sys=" + sys + "&pid=" + pid + tag)
			}
		</script>
	</head>
	<body bgcolor="#8FBC8F">
		<div id="topDiv">
		<h3>PM.sys dmp - Content Details</h3>
		<form method="GET" action="detailContent.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
echo "		<div id=\"lDIV\" class=\"linking\">\n";
echo "			<a href=\"../?sys=" . $sys . "\">Hauptmen&uuml;</a><br>\n";
echo "			<a href=\"../pages/?sys=" . $sys . "\">Zur Seiten &Uuml;bersicht</a><br>\n";
echo "		</div>\n";
include "../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$q = sprintf("SELECT ID, Alias FROM Page%s ORDER BY Alias;", $devTable);
$r = mysqli_query($DBcon, $q);
echo "		<div id=\"lDIV\" class=\"cEdit\">\n";
echo "		<form method=\"GET\" action=\"detailContent.php\">\n";
echo "			Seite:<select name=\"pid\">\n";
while($pages = mysqli_fetch_array($r)){
	echo "				<option value=\"" . $pages["ID"] . "\"";
	if(isset($_GET["pid"])){
		if($_GET["pid"] == $pages["ID"]){echo " selected";}
	}
	echo ">" . $pages["Alias"] . "</option>\n";	
}
echo "			</select>\n";
echo "			<input type=\"submit\" value=\"OK\">\n";
echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
echo "		</form>\n";
?>

		</div>
		</div><!-- End of Top DIV -->
		<div id="tDIV">
<?php 
if(isset($_GET["pid"])){
	echo "			<a href=\"add/addform.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "\"><input type=\"button\" value=\"Neuen Content anlegen\"></a>\n"; # no br afterwards!!!
}
?>
			<table>
				<thead>
					<tr>
						<th>Area Name</th>
<?php
echo "						<th>\n";
echo "								HTML Tag Type\n";
echo "								<select id=\"TagOption\" onchange=\"FilterTagType()\">\n";
$tag = "";
echo "									<option value=\"*\"";
if(isset($_GET["tag"])){
	$tag=$_GET["tag"];
} else {
	echo " selected";
}
echo ">*</option>\n";
$opts = array('naked', "p", "li", "H1", "H2", "H3", "H4", "H5", "H6");
$optcnt=count($opts);
for ($i=0; $i < $optcnt; $i++) { 
	echo "									<option value=\"" . $opts[$i] . "\"";
	if($tag == $opts[$i]){ echo " selected";}
	echo ">" . $opts[$i] . "</option>\n";
}
echo "								</select>\n";
echo "						</th>\n";
?>
						<th>Sprachen</th>
						<th>Optionen</th>
					</tr>
				</thead>
				<tbody>
<?php
if(isset($_GET["pid"])){
	$wh_tag="";
	if(strlen($tag) > 0){
		$wh_tag = sprintf(" AND HTML_Type='%s'", $tag);
	}
	$areaQ = sprintf("SELECT * FROM Area%s WHERE PageID=%d%s;", $devTable, $_GET["pid"], $wh_tag);
	$areaR = mysqli_query($DBcon, $areaQ);
	echo "<!-- " . $areaQ . " -->\n";
	while ($area = mysqli_fetch_array($areaR)) {
		$langQ = sprintf("SELECT l.DisplayValue,dl.lang FROM displaylang%s dl join Language l on dl.Area='%s' and dl.lang=l.abbreviation and dl.PageID=%d;", $devTable, $area["Name"], $_GET["pid"]);
		$langR = mysqli_query($DBcon, $langQ);
		echo "					<tr>\n";
		echo "						<td>" . $area["Name"] . "</td>\n";
		echo "						<td>" . $area["HTML_Type"] . "</td>\n";
		echo "						<td>\n";
		while ($lang = mysqli_fetch_array($langR)) {
			echo "						" . $lang["DisplayValue"] . "&emsp;";
		}
		echo "						</td>\n";
		echo "						<td>\n";
		if ($area["HTML_Type"] == 'p') {
			echo "							<a href=\"modP.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "&aname=" . urlencode($area["Name"]) . "\"><input type=\"button\" value=\"Absatz &auml;ndern\"></a>\n";
		} else {
			echo "							<a href=\"field/?sys=" . $sys . "&pid=" . $_GET["pid"] . "&aname=" . urlencode($area["Name"]) . "\"><input type=\"button\" value=\"Text &auml;ndern\"></a>\n";
		}
		echo "							<a href=\"delArea.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "&aname=" . urlencode($area["Name"]) . "\"><input type=\"button\" value=\"Entfernen\"></a>\n";
		echo "						</td>\n";
		echo "					</tr>\n";
	}
}
mysqli_close($DBcon);
?>
				</tbody>
			</table>	
		</div>	
	</body>
</html>
