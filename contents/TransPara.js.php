<?php
header('Content-Type: text/javascript');
?>
function sysparam(){
	return <?php echo "\"" . "sys=" . $_GET["sys"] . "&pid=" . $_GET["pid"] . "\""; ?>;
}
function DoTranslate(aname) {
	xmlhttp = new XMLHttpRequest();
	btn = document.getElementById('btnGO');
	area = document.getElementById('area');
	btn.disabled = true;
	to_lang=document.getElementById('lang').value;
	from_lang = document.getElementById('translate').value;
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			area.innerHTML = this.responseText;
			btn.disabled = false;
			AreaHasChanged();
		} else if (this.readyState == 4 && this.status != 200) {
			alert('Something went wrong: ' + this.status);
			btn.disabled = false;
		}
	}
	xmlhttp.open("GET", "para/translate.php?aname=" + encodeURI(aname) + "&" + sysparam() + "&from=" + from_lang + "&to=" + to_lang, true);
    xmlhttp.send();
}