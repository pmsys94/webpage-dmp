<?php
include "../../database/db_write_condat.inc";
$descriptorspec = array(
   0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
   1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
   2 => array("pipe", "w"),  // stderr is a pipe that the child will write to
);
$cmd = 'sudo -EHu dmp-py-trans /home/dmp-py-trans/trans.py ' . $_GET['to'] . ' ' . $_GET['from'] . ' ' . urldecode($_GET['aname']) . ' ' . $_GET['pid'];
if ($_GET['sys'] == 'test') {
	$cmd .= ' --dev';
}
$condata = "server=" . $DBserver . "\nuser=" . $DBuser . "\npassword=" . $DBpass . "\nDB=" . $DBname;
$process = proc_open($cmd, $descriptorspec, $pipes, null, $_SERVER);
if (is_resource($process)) {
    // $pipes now looks like this:
    // 0 => writeable handle connected to child stdin
    // 1 => readable handle connected to child stdout
    // 2 => readable handle connected to child stderr

    fwrite($pipes[0], $condata);
    fclose($pipes[0]);

    $user_res = stream_get_contents($pipes[1]);
    fclose($pipes[1]);
    $err_res = "DMP Translator py - STDERR: " . stream_get_contents($pipes[2]);
    fclose($pipes[2]);

    // It is important that you close any pipes before calling
    // proc_close in order to avoid a deadlock
    $return_value = proc_close($process);

    if ($return_value != 0) {
    	error_log($err_res);
    	error_log("Pocess call returned with : " . $return_value);
    	http_response_code(500);
    }
} else {
	error_log('Process creation failed');
	http_response_code(500);
}
echo $user_res;
?>