<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Absatz L&ouml;schen</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	if(isset($_GET["sys"])){
		if($_GET["sys"] == "des"){
				$devTable = "";
		}
		if($_GET["sys"] == "test"){
				$devTable = "DEV";
		}
		$sys = $_GET["sys"];
	} else {
		$sys = "des";
		$devTable = "";
	}
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
}
include "../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
if($_SERVER["REQUEST_METHOD"] == "GET"){
	echo "		<form method=\"POST\" action=\"delArea.php\">\n";
	echo "			<h4 style=\"color: red;\">Wollen sie die Area wirklich l&ouml;schen?</h4>\n";
	echo "			<b>" . urldecode($_GET["aname"]) . "</b><br>\n";
	echo "			<input type=\"submit\" value=\"Ja\">\n";
	echo "			<a href=\"detailContent.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "\"><input type=\"button\" value=\"Nein\"></a><br>\n";
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"detailContent.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"pid\" value=\"" . $_GET["pid"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
	echo "			<input type=\"hidden\" name=\"aname\" value=\"" . urldecode($_GET["aname"]) . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	$dQ = sprintf("DELETE FROM displaylang%s WHERE Area = '%s' AND PageID = %d;", $devTable, $_POST["aname"], $_POST["pid"]);
	if(mysqli_query($DBcon, $dQ)){
		echo "		Erfolg bei entfernen der Bodys!<br>\n";
		mysqli_commit($DBcon);
	} else {
		echo "		Fehler: " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
	$dQ = sprintf("DELETE FROM Area%s WHERE Name = '%s' AND PageID = %d;", $devTable, $_POST["aname"], $_POST["pid"]);
	if(mysqli_query($DBcon, $dQ)){
		echo "		Erfolg bei der Area!<br>\n";
		mysqli_commit($DBcon);
	} else {
		echo "		Fehler: " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
}
mysqli_close($DBcon);
?>
	</body>
</html>
