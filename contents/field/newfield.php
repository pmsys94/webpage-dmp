<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Feld &Uuml;bersetzung hinzuf&uuml;gen</h3>
		<form method="GET" action="newfield.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"hidden\" name=\"pid\" value=\"" . $_GET["pid"] . "\">\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
echo "		<br><br>\n";
echo "		<form method=\"POST\" action=\"addfield.php\">\n";
echo "			Sprache:<select name=\"lang\">\n";
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$langsQ = sprintf("SELECT abbreviation AS lang, DisplayValue FROM Language WHERE abbreviation NOT IN (SELECT lang FROM displaylang%s WHERE Area='%s' AND PageID=%d) AND NOT abbreviation='all';", $devTable, urldecode($_GET["aname"]), $_GET["pid"]);
$langR = mysqli_query($DBcon, $langsQ);
$langC = mysqli_num_rows($langR);
if($langC > 0){
	while ($lang = mysqli_fetch_array($langR)) {
		echo "				<option value=\"" . $lang["lang"] . "\">" . $lang["DisplayValue"] . "</option>\n";
	}
} else {
	echo "				<option>Keine Sprachen verfügabar</option>\n";
}
echo "			</select>\n";
echo "			<br>\n";
echo "			Area: " . htmlentities(urldecode($_GET["aname"])) . "<input type=\"hidden\" name=\"aname\" value=\"" . urldecode($_GET["aname"]) . "\"><br>\n";
echo "			Feld Wert: <input type=\"text\" name=\"body\"><br>\n";
echo "			<input type=\"submit\" value=\"Anlegen\"";
if ($langC == 0){
	echo " disabled";
}
echo ">\n";
echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
echo "			<input type=\"hidden\" name=\"pid\" value=\"" . $_GET["pid"] . "\">\n";
echo "			<a href=\"index.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "&aname=" . $_GET["aname"] . "\"><input type=\"button\" name=\"btn_cncl\" value=\"Abbrechen\"></a>\n";
mysqli_close($DBcon);
?>	
		</form>
	</body>
</html>