<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Field &Uuml;bersetzung anlegen</h3>
<?php
if(isset($_POST["sys"])){
	if($_POST["sys"] == "des"){
			$devTable = "";
	}else if($_POST["sys"] == "test"){
			$devTable = "DEV";
	}
	$sys = $_POST["sys"];
} else {
	echo "				ERROR: No System by POST!!!<br>\n";
	die("");
}
echo "		<a href=\"index.php?sys=" . $sys . "&pid=" . $_POST["pid"] . "&aname=" . $_POST["aname"] . "\">Zur&uuml;ck</a>\n";
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
mysqli_autocommit($DBcon, FALSE);

$ccQ = sprintf("INSERT INTO displaylang%s(Area, lang, TXT_Body, PageID) VALUES ('%s', '%s', ?, %d);", $devTable, $_POST["aname"], $_POST["lang"], $_POST["pid"]);
$prep = mysqli_prepare($DBcon, $ccQ);
if($prep){
	if(mysqli_stmt_bind_param($prep, "s", $_POST["body"])){
		if(mysqli_stmt_execute($prep)){
			echo "					Erfolg!<br>\n";
			mysqli_commit($DBcon);
		} else {
			echo "					Fehler beim Ausführen: " . mysqli_error($DBcon) . "<br>\n";
			mysqli_rollback($DBcon);
		}
	} else {
		echo "					Fehler beim Binden: " . mysqli_error($DBcon) . "<br>\n";	
	}
	mysqli_stmt_close($prep);
} else {
	echo "					Fehler beim Vorbereiten: " . mysqli_error($DBcon) . "<br>\n";
}
mysqli_close($DBcon);
?>
	</body>
</html>
