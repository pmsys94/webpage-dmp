<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<script type="text/javascript">
			function DevTable(){
				return <?php echo "\"" . $_GET["sys"] . "\"";?>;
			}
			function sysparam(){
				return <?php echo "\"" . "sys=" . $_GET["sys"] . "&pid=" . $_GET["pid"] . "\""; ?>;
			}
			function UpdateTXT(aname, lang) {
				entry = document.getElementById("txt_" + lang).value;
				pid = <?php echo $_GET["pid"]; ?>;
				obj = { "entry":entry, "aname":aname, "lang":lang, "sys":DevTable(), "pid":pid };
				sendobj = JSON.stringify(obj);
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("index.php?" + sysparam() + "&aname=" + aname);
       					}
       				}
				}
				xmlhttp.open("POST", "UpdateField.php", true);
       			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
       			xmlhttp.send("x=" + encodeURIComponent(sendobj));
			}
			function DelTXT(aname, lang) {
				if(confirm("Soll die Übersetzung wirklich gelöscht werden?") == false){return;}
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("index.php?" + sysparam() + "&aname=" + aname);
       					}
       				}
				}
				xmlhttp.open("GET", "DelField.php?aname=" + encodeURIComponent(aname) + "&lang=" + lang + "&" + sysparam(), true);
                xmlhttp.send();

			}
			function changeTXT(lang){
				document.getElementById("btn_" + lang).disabled = false;
			}
		</script>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Text &Auml;ndern</h3>
<?php
if(isset($_GET["sys"])){
	if($_GET["sys"] == "des"){
			$devTable = "";
	}
	if($_GET["sys"] == "test"){
			$devTable = "DEV";
	}
	$sys = $_GET["sys"];
} else {
	$sys = "des";
	$devTable = "";
}
echo "		<a href=\"../../?sys=" . $sys . "\">Hauptmen&uuml;</a><br>\n";
echo "		<a href=\"../detailContent.php?sys=" . $sys . "&pid=" . $_GET["pid"] . "\">Content &Uuml;bersicht</a><br>\n";
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
echo "			<a href=\"newfield.php?sys=" . $sys . "&aname=" . $_GET["aname"] . "&pid=" . $_GET["pid"] . "\"><input type=\"button\" value=\"Neue &Uuml;bersetzung anlegen\"></a>\n"; # no br afterwards!!!
$pageQ = sprintf("SELECT Alias FROM Page%s WHERE ID = %d;", $devTable, $_GET["pid"]);
$pageR = mysqli_query($DBcon, $pageQ);
$page = mysqli_fetch_array($pageR);
echo "		<h4>Seite: " . $page["Alias"] . "</h4>\n";
echo "		<h4>Feld: " . urldecode($_GET["aname"]) . "</h4>\n";
?>
			<table>
				<thead>
					<tr>
						<th>Sprache</th>
						<th>Text</th>
					</tr>
				</thead>
				<tbody>
<?php
	$areaQ = sprintf("SELECT l.DisplayValue,dl.* FROM displaylang%s dl join Language l on dl.Area='%s' and dl.lang=l.abbreviation and dl.PageID=%d;", $devTable, urldecode($_GET["aname"]), $_GET["pid"]);
	$areaR = mysqli_query($DBcon, $areaQ);
	while ($area = mysqli_fetch_array($areaR)) {
		echo "					<tr>\n";
		echo "						<td>" . $area["DisplayValue"] . "</td>\n";
		echo "						<td>\n";
		echo "							<input type=\"text\" id=\"txt_" . $area["lang"] . "\" value=\"" . htmlentities($area["TXT_Body"]) . "\" onchange=\"changeTXT('" . $area["lang"] . "')\">\n";
		echo "							<input type=\"button\" id=\"btn_" . $area["lang"] . "\" value=\"&Auml;ndern\" onclick=\"UpdateTXT('" . urldecode($_GET["aname"]) . "', '" . $area["lang"] . "')\" disabled>\n";
		echo "							<input type=\"button\" value=\"L&ouml;schen\" onclick=\"DelTXT('" . urldecode($_GET["aname"]) . "', '" . $area["lang"] . "')\">\n";
		echo "						</td>\n";
		echo "					</tr>\n";
	}
mysqli_close($DBcon);
?>
				</tbody>
			</table>	
	</body>
</html>
