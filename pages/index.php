<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<style type="text/css">
			ul{
				margin-top: 0px;
				margin-bottom: 0px;
			}
		</style>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Seiten &Uuml;bersicht</h3>
		<form method="GET" action="index.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
echo "		<a href=\"/excl/dmp/index.php?sys=" . $sys . "\">Hauptmen&uuml;</a><br>\n";
echo "		<a href=\"add/addform.php?sys=" . $sys . "\"><input type=\"button\" value=\"Neue Seite anlegen\"></a>\n";
?>
		<br><br>
		<table>
			<thead>
				<tr>
					<th>Page ID</th>
					<th>Alias</th>
					<th>Sprachen</th>
					<th>Men&uuml; Rekursion</th>
					<th>Highlight Men&uuml; Item</th>
					<th>Optionen</th>
				</tr>
			</thead>
			<tbody>
<?php
include "../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$q = sprintf("SELECT * FROM Page%s;", $devTable);
$r = mysqli_query($DBcon, $q);
while($row = mysqli_fetch_array($r)){
	$langQ = sprintf("SELECT DisplayValue FROM Language join speaks%s ON abbreviation=lang WHERE Page = '%s';", $devTable, $row["ID"]);
	$langR = mysqli_query($DBcon, $langQ);
	echo "				<tr>\n";
	echo "					<td><a href=\"../contents/detailContent.php?sys=" . $sys . "&pid=" . $row["ID"] . "\">" . $row["ID"] . "</a></td>\n";
	echo "					<td>" . $row["Alias"] . "</td>\n";
	echo "					<td><ul>\n";
	while ($lang = mysqli_fetch_array($langR)) {
	 	echo "						<li>". $lang["DisplayValue"] . "</li>\n";
	}
	echo "					</ul></td>\n";
	echo "					<td>" . $row["PointsMenu"] . "</td>\n";
	if ($row["HighlightsItem"] != "0") {
		$mitQ = sprintf("SELECT EntryText FROM translates%s t WHERE MenuItem = %d;", $devTable, $row["HighlightsItem"]);
		$mitR = mysqli_query($DBcon, $mitQ);
		$menIT = mysqli_fetch_array($mitR);
		echo "					<td>" . $menIT["EntryText"] . "</td>\n";
	} else {
		echo "					<td>keins</td>\n";
	}
	echo "					<td>\n";
	echo "						<a href=\"add/addform.php?sys=" . $sys . "&call=copy&id=" . $row["ID"] . "\"><input type=\"button\" value=\"Neue Seite aus dieser Kopieren\"></a>\n";
	echo "						<a href=\"title/view.php?sys=" . $sys . "&pid=" . $row["ID"] . "\"><input type=\"button\" value=\"Title Tag &Auml;ndern\"></a>\n";
	echo "						<a href=\"changeMenu.php?sys=" . $sys . "&pid=" . $row["ID"] . "\"><input type=\"button\" value=\"Men&uuml; beziehung &auml;ndern\"></a>\n";
	echo "						<a href=\"delpage.php?sys=" . $sys . "&pid=" . $row["ID"] . "\"><input type=\"button\" value=\"L&ouml;schen\"></a>\n";
	echo "					</td>\n";
	echo "				</tr>\n";
}
mysqli_close($DBcon);
?>
			</tbody>
		</table>	
	</body>
</html>
