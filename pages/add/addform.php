<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
<?php echo "		<script type=\"text/javascript\" src=\"item_choice.js.php?sys=" . $_GET["sys"] . "&call=new&relative=.\"></script>\n";?>
	</head>
	<body bgcolor="#8FBC8F" onload="DocLoaded()">
		<h3>PM.sys dmp - Neue Seite Anlegen</h3>
		<form method="GET" action="addform.php">
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select>\n";
echo "			<input type=\"submit\">\n";
echo "		</form><br>\n";
?>
		<br><br>
		<form method="POST" action="addaction.php">
<?php
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$mxIDq = sprintf("SELECT max(ID)+1 AS mxID FROM Page%s", $devTable);
$mxIDr = mysqli_query($DBcon, $mxIDq);
if($mxIDr === FALSE){
	$pid = "-1";
} else {
	$mxID = mysqli_fetch_array($mxIDr);
	if(!is_null($mxID["mxID"])){
		$pid = $mxID["mxID"];
	} else{
		$pid = "1";
	}
}
$alias = "";
$ht_title = "";
echo "			Page ID:";
echo "<input type=\"text\" name=\"pid\" value=\"" . $pid . "\" required><br>\n";
$langQ = "SELECT * FROM Language where NOT abbreviation='all';";
echo "			Page Alias:<input type=\"text\" name=\"alias\" value=\"" . $alias . "\"><br>\n";
echo "			Sprache:<select name=\"lang\" required>\n";
$langR = mysqli_query($DBcon, $langQ);
while ($langs = mysqli_fetch_array($langR)) {
	echo "				<option value=\"" . $langs["abbreviation"] . "\"";
	if($langs["abbreviation"] == "de"){
		echo " selected";
	}
	echo ">" . $langs["DisplayValue"] . "</option>\n";
}
echo "			</select><br>\n";
echo "			Title Tag:<input type=\"text\" name=\"ht_title\" value=\"" . $ht_title . "\" size=\"100\"><br>\n";
echo "			Men&uuml; Rekursion:<select name=\"men\" id=\"men\" onchange=\"MenuChosen('')\" required>\n";
$menuQ = sprintf("SELECT * FROM Menu%s", $devTable);
$menuR = mysqli_query($DBcon, $menuQ);
while ($menus = mysqli_fetch_array($menuR)) {
	echo "				<option value=\"" . $menus["MenuKey"] . "\">" . $menus["MenuKey"] . "</option>\n";
}
echo "			</select><br>\n";
echo "			Men&uuml; Highlighten:<select name=\"item\" id=\"items\">\n";
echo "			</select> (nur Deutsche Auswahl, da nur ID)<br>\n";
mysqli_close($DBcon);
echo "			<input type=\"submit\">\n";
echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
echo "			<a href=\"../index.php?sys=" . $sys . "\"><input type=\"button\" name=\"btn_cncl\" value=\"Abbrechen\"></a>\n";
?>	
		</form>
		<script type="text/javascript">
			function DocLoaded(){
				MenuChosen('');
			}
		</script>
	</body>
</html>