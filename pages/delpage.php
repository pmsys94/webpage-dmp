<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Seite L&ouml;schen</h3>
<?php
if($_SERVER["REQUEST_METHOD"] == "GET"){
	if(isset($_GET["sys"])){
		if($_GET["sys"] == "des"){
				$devTable = "";
		}
		if($_GET["sys"] == "test"){
				$devTable = "DEV";
		}
		$sys = $_GET["sys"];
	} else {
		$sys = "des";
		$devTable = "";
	}
} else {
	echo "		<a href=\"" . $_POST["retlink"] . "\">Zur&uuml;ck</a>\n";
	if($_POST["devtable"] > 0){
		$devTable = "DEV";
	} else {
		$devTable = "";
	}
}
include "../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
if($_SERVER["REQUEST_METHOD"] == "GET"){
	$pageQ = sprintf("SELECT Alias FROM Page%s WHERE ID = %d;", $devTable, $_GET["pid"]);
	$pageR = mysqli_query($DBcon, $pageQ);
	$page = mysqli_fetch_array($pageR);
	echo "		<form method=\"POST\" action=\"delpage.php\">\n";
	echo "			<h4 style=\"color: red;\">Wollen sie die Seite wirklich l&ouml;schen?</h4>\n";
	echo "			<b>" . $page["Alias"] . "</b><br>\n";
	echo "			<input type=\"submit\" value=\"Ja\">\n";
	echo "			<a href=\"index.php?sys=" . $sys . "\"><input type=\"button\" value=\"Nein\"></a><br>\n";
	echo "			<input type=\"hidden\" name=\"retlink\" value=\"index.php?sys=" . $sys . "\">\n";
	echo "			<input type=\"hidden\" name=\"devtable\" value=\"" . strlen($devTable) . "\">\n";
	echo "			<input type=\"hidden\" name=\"pid\" value=\"" . $_GET["pid"] . "\">\n";
	echo "			<input type=\"hidden\" name=\"sys\" value=\"" . $sys . "\">\n";
	echo "		</form>\n";
} else {
	mysqli_autocommit($DBcon, FALSE);
	echo "			Killing Area Bodys...\n";
	$dQ = sprintf("DELETE FROM displaylang%s WHERE Area IN (SELECT Name FROM Area%s WHERE PageID = %d) AND PageID = %d;", $devTable, $devTable, $_POST["pid"], $_POST["pid"]);
	if(mysqli_query($DBcon, $dQ)){
		echo "[ <b> OK </b> ]<br>\n";
	} else {
		echo "[ <b style=\"color: orange;\"> FAIL </b> ] " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
	echo "			Killing Areas...\n";
	$dQ = sprintf("DELETE FROM Area%s WHERE PageID = %d;", $devTable, $_POST["pid"]);
	if(mysqli_query($DBcon, $dQ)){
		echo "[ <b> OK </b> ]<br>\n";
	} else {
		echo "[ <b style=\"color: orange;\"> FAIL </b> ] " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
	echo "			Removing speaks relation...\n";
	$dQ = sprintf("DELETE FROM speaks%s WHERE Page = %d;", $devTable, $_POST["pid"]);
	if(mysqli_query($DBcon, $dQ)){
		echo "[ <b> OK </b> ]<br>\n";
	} else {
		echo "[ <b style=\"color: orange;\"> FAIL </b> ] " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
	echo "			Remove Page...\n";
	$dQ = sprintf("DELETE FROM Page%s WHERE ID = %d;", $devTable, $_POST["pid"]);
	if(mysqli_query($DBcon, $dQ)){
		echo "[ <b> OK </b> ]<br>\n";
	} else {
		echo "[ <b style=\"color: orange;\"> FAIL </b> ] " . mysqli_error($DBcon) . "<br>\n";
		mysqli_rollback($DBcon);
	}
	mysqli_commit($DBcon);
}
mysqli_close($DBcon);
?>
	</body>
</html>
