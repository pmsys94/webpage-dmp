<!DOCTYPE html>
<html>
	<head>	
		<title>dmp - PM.sys</title>
		<link rel="icon" href="/pm_fav.ico">
		<link rel="stylesheet" type="text/css" href="/excl/dmp/styles/dmp-tables.css">
		<script type="text/javascript">
			function DevTable(){
				return <?php echo "\"" . $_GET["sys"] . "\"";?>;
			}
			function sysparam(){
				return <?php echo "\"" . "sys=" . $_GET["sys"] . "&pid=" . $_GET["pid"] . "\""; ?>;
			}
			function storeTitle(page, lang, action) {
				if(action == "insert"){
					entry = document.getElementById("txt_new").value;
					lang = document.getElementById("sel_new").value;
				} else {
					entry = document.getElementById("txt_" + lang).value;
				}
				obj = { "entry":entry, "page":page, "lang":lang, "sys":DevTable(), "act":action };
				sendobj = JSON.stringify(obj);
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("view.php?" + sysparam());
       					}
       				}
				}
				xmlhttp.open("POST", "storeTitle.php", true);
       			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
       			xmlhttp.send("x=" + sendobj);
			}
			function delTitle(lang) {
				if(confirm("Soll der Title Tag wirklich gelöscht werden?") == false){return;}
				xmlhttp = new XMLHttpRequest();
       			xmlhttp.onreadystatechange = function() {
       				if (this.readyState == 4 && this.status == 200) {
       					if(this.responseText != "0"){
       						alert(this.responseText);
       					} else {
       						window.location.assign("view.php?" + sysparam());
       					}
       				}
				}
				xmlhttp.open("GET", "DelTitle.php?" + sysparam() + "&lang=" + lang, true);
                xmlhttp.send();

			}
			function TXT_Change(lang){
				document.getElementById("btn_" + lang).disabled = false;
			}
		</script>
	</head>
	<body bgcolor="#8FBC8F">
		<h3>PM.sys dmp - Seite - HTML Title</h3>
<?php
echo "			System:<select name=\"sys\">\n";
if(isset($_GET["sys"])){
	echo "					<option value=\"des\"";
	if($_GET["sys"] == "des"){
			echo " selected";
			$devTable = "";
	}
	echo ">design</option>\n";
	echo "				<option value=\"test\"";
	if($_GET["sys"] == "test"){
			echo " selected";
			$devTable = "DEV";
	}
	echo ">dmp testing</option>\n";
	$sys = $_GET["sys"];
} else {
	echo "				<option value=\"des\" selected>design</option>\n";
	echo "				<option value=\"test\">dmp testing</option>\n";
	$sys = "des";
	$devTable = "";
}
echo "			</select><br>\n";
echo "		<a href=\"/excl/dmp/index.php?sys=" . $sys . "\">Hauptmen&uuml;</a><br><br><br>\n";
echo "		<a href=\"../index.php?sys=" . $sys . "\">Zur&uuml;ck</a>\n";
include "../../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$pageQ = sprintf("SELECT Alias FROM Page%s WHERE ID = %d;", $devTable, $_GET["pid"]);
$pageR = mysqli_query($DBcon, $pageQ);
$page = mysqli_fetch_array($pageR);
echo "		<h4>Seite: " . $page["Alias"] . "</h4>\n";
$langsQ = "SELECT * FROM Language WHERE abbreviation != 'all';";
$langsR = mysqli_query($DBcon, $langsQ);
echo "		<select id=\"sel_new\">\n";
while ($lang = mysqli_fetch_array($langsR)) {
	echo "			<option value=\"" . $lang["abbreviation"] . "\">" . $lang["DisplayValue"] . "</option>\n";
}
echo "		</select>\n";
echo "		<input type=\"text\" id=\"txt_new\">\n";
echo "		<input type=\"button\" value=\"Neue Title anlegen\" onclick=\"storeTitle('" . $_GET["pid"] . "', '', 'insert')\">\n";
?>
		<br><br>
		<table>
			<thead>
				<tr>
					<th>Sprachen</th>
					<th>Title</th>
				</tr>
			</thead>
			<tbody>
<?php
$q = sprintf("SELECT sp.*,l.DisplayValue FROM speaks%s sp join Language l ON abbreviation=lang WHERE Page = %d;", $devTable, $_GET["pid"]);
$r = mysqli_query($DBcon, $q);
while($row = mysqli_fetch_array($r)){
	echo "				<tr>\n";
	echo "					<td>" . $row["DisplayValue"] . "</td>\n";
	echo "					<td>\n";
	echo "						<input type=\"text\" id=\"txt_" . $row["lang"] . "\" value=\"" . $row["HTML_Title"] . "\" onchange=\"TXT_Change('" . $row["lang"] . "')\">\n";
	echo "						<input type=\"button\" id=\"btn_" . $row["lang"] . "\" value=\"Title &auml;ndern\" onclick=\"storeTitle('" . $_GET["pid"] . "', '" . $row["lang"] . "', 'update')\" disabled>\n";
	echo "						<input type=\"button\" value=\"L&ouml;schen\" onclick=\"delTitle('" . $row["lang"] . "')\">\n";
	echo "					</td>\n";
	echo "				</tr>\n";
}
mysqli_close($DBcon);
?>
			</tbody>
		</table>	
	</body>
</html>
