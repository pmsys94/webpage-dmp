<?php
echo "<p>\n";
echo "Try to remove langs:<br>\n";
$langRf = "SELECT abbreviation FROM pmsys.Language WHERE abbreviation NOT IN (SELECT abbreviation from pmsysDEV.Language);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Langs to remove " . mysqli_num_rows($langRr) . "<br>\n";
	echo "Removing: ";
	while ($langs = mysqli_fetch_array($langRr)) {
		echo $langs["abbreviation"];
	}
	echo "<br>\n";
} else {
	echo "Can not fetch removed langs: " . mysqli_error($DBcon) . "<br>\n";
}
$langRq = "DELETE FROM pmsys.Language WHERE abbreviation NOT IN (SELECT abbreviation FROM pmsysDEV.Language);";
$langRe = mysqli_query($DBcon, $langRq);
if($langRe){
	echo "Removed " . mysqli_affected_rows($DBcon) . " langs.<br>\n";
} else {
	echo "Error in remove: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;

}
echo "</p>";
?>