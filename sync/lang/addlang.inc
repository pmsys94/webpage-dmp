<?php
echo "<p>\n";
echo "Try to add missing Languages...<br>\n";
$langRf = "SELECT abbreviation FROM pmsysDEV.Language WHERE abbreviation NOT IN (SELECT abbreviation FROM pmsys.Language);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Langs to add " . mysqli_num_rows($langRr) . "<br>\n";
	echo "Adding: ";
	while ($langs = mysqli_fetch_array($langRr)) {
		echo $langs["abbreviation"] . ", ";
	}
	echo "<br>\n";
} else {
	echo "Can not fetch langs to add: " . mysqli_error($DBcon) . "<br>\n";
}
$langAq = "INSERT INTO pmsys.Language(abbreviation, DisplayValue) SELECT abbreviation, DisplayValue FROM pmsysDEV.Language WHERE abbreviation NOT IN (SELECT abbreviation FROM pmsys.Language);";
$langAe = mysqli_query($DBcon, $langAq);
if ($langAe) {
	echo "Added " . mysqli_affected_rows($DBcon) . "<br>\n";
} else {
	echo "Error on add: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;
}
echo "</p>";
?>