<?php
echo "<p>\n";
echo "Try to remove deleted Pages.<br>\n";
echo "Remove of relation to Language...";
$dSq = "DELETE FROM pmsys.speaks WHERE Page NOT IN (SELECT Page FROM pmsysDEV.speaks);";
$dSr = mysqli_query($DBcon, $dSq);
if($dSr){
	echo "OK<br>Removed " . mysqli_affected_rows($DBcon) . " Relations.<br>\n";
} else {
	echo "Failed. ". mysqli_error($DBcon) . "<br>\n";
}
$langRf = "SELECT ID, Alias FROM pmsys.Page WHERE NOT ID IN (SELECT ID from pmsysDEV.Page);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Pages " . mysqli_num_rows($langRr) . " to remove.<br>\n";
} else {
	echo "Can not fetch removed Pages: " . mysqli_error($DBcon) . "<br>\n";
}
$langRq = "DELETE FROM pmsys.Page WHERE NOT ID IN (SELECT ID FROM pmsysDEV.Page);";
$langRe = mysqli_query($DBcon, $langRq);
if($langRe){
	echo "Removed " . mysqli_affected_rows($DBcon) . " Pages.<br>\n";
} else {
	echo "Error in remove: " . mysqli_error($DBcon) . "<br>\n";
	echo "May need to sync Areas First, if removed areas depends on this Content.<br>\n";
	$exit = 1;

}
echo "</p>";
?>