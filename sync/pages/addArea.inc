<?php
echo "<p>\n";
echo "Try to add missing Areas...<br>\n";
$langAq = "INSERT INTO pmsys.Area(Name, HTML_Type, PageID) SELECT Name, HTML_Type, PageID FROM pmsysDEV.Area s WHERE NOT EXISTS (SELECT 1 FROM pmsys.Area t WHERE s.Name=t.Name and s.PageID=t.PageID);";
$langAe = mysqli_query($DBcon, $langAq);
if ($langAe) {
	echo "Added " . mysqli_affected_rows($DBcon) . " Areas.<br>\n";
} else {
	echo "Error on add Areas: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;
}

$iDq = "INSERT INTO pmsys.displaylang(Area,lang,TXT_Body,PageID) SELECT Area,lang,TXT_Body,PageID FROM pmsysDEV.displaylang s WHERE NOT EXISTS (SELECT 1 FROM pmsys.displaylang t WHERE s.PageID=t.PageID AND s.Area = t.Area);";
$iDr = mysqli_query($DBcon, $iDq);
if($iDr){
	echo "Added " . mysqli_affected_rows($DBcon) . " Relations.<br>\n";
} else {
	echo "Can not add Relations: " . mysqli_error($DBcon) . "<br>\n";
}
echo "</p>";
?>