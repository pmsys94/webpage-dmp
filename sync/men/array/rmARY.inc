<?php
echo "<p>\n";
echo "Try to remove deleted Arrays:<br>\n";
$langRf = "SELECT MenuKey FROM pmsys.Menu WHERE MenuKey NOT IN (SELECT MenuKey from pmsysDEV.Menu);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Menus to remove " . mysqli_num_rows($langRr) . "<br>\n";
	while ($langs = mysqli_fetch_array($langRr)) {
		echo "Removing: " . $langs["MenuKey"] . "<br>\n";
		$cDCq = "SELECT count(PointsMenu) AS cnt FROM pmsys.Content WHERE PointsMenu ='" - $langs["MenuKey"] . "';";
		$cDCr = mysqli_query($DBcon, $cDCq);
		if($cDCr){
			$cDC = mysqli_fetch_array($cDCr);
			if($cDC["cnt"] != 0){
				echo "Can not Continue bec. a Content Points to Menu. Set in Content, Sync Contents and after sync Menus again!<br>\n";
				$exit = 1;
				break;
			}
		} else {
			echo "Can not check for dep. Content to this Menu! Abort...<br>\n";
			$exit = 1;
			break;
		}
		echo "Set all Submenu Pointer to Arthm. 0: ";
		$SP0q = "UPDATE pmsys.MenuItem SET Submenu = '0' WHERE Submenu = '" . $langs["MenuKey"] . "';";
		$SP0r = mysqli_query($DBcon, $SP0q);
		if($SP0r){
			echo mysqli_affected_rows($DBcon) . " affected.<br>\n";
		} else {
			echo "Failure on set - " . mysqli_error($DBcon) . "<br>\n";
		}
		echo "Also delete dependent Menu Items: ";
		$dDIq = "DELETE FROM pmsys.MenuItem WHERE ConnectedMenu = '" . $langs["MenuKey"] . "';";
		$dDIr = mysqli_query($DBcon, $dDIq);
		if($dDIr){
			echo mysqli_affected_rows($DBcon) . " affected Items.<br>\n";
		} else {
			echo "Failure on del. dep. - " . mysqli_error($DBcon) . "<br>\n";
			$exit = 1;
		}
	}
} else {
	echo "Can not fetch removed Menus: " . mysqli_error($DBcon) . "<br>\n";
}
if($exit){
	mysqli_rollback($DBcon);
	mysqli_close($DBcon);
	die();
}
$langRq = "DELETE FROM pmsys.Menu WHERE MenuKey NOT IN (SELECT MenuKey FROM pmsysDEV.Menu);";
$langRe = mysqli_query($DBcon, $langRq);
if($langRe){
	echo "Removed " . mysqli_affected_rows($DBcon) . " Menus.<br>\n";
} else {
	echo "Error in remove: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;

}
echo "</p>";
?>