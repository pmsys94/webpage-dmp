<?php
echo "<p>\n";
echo "Try to add missing Menus...<br>\n";
$langRf = "SELECT MenuKey FROM pmsysDEV.Menu WHERE MenuKey NOT IN (SELECT MenuKey FROM pmsys.Menu);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Menus to add " . mysqli_num_rows($langRr) . "<br>\n";
	echo "Adding: ";
	while ($langs = mysqli_fetch_array($langRr)) {
		echo $langs["MenuKey"] . ", ";
	}
	echo "<br>\n";
} else {
	echo "Can not fetch langs to add: " . mysqli_error($DBcon) . "<br>\n";
}
$langAq = "INSERT INTO pmsys.Menu(MenuKey, SuperiorKey) SELECT MenuKey, SuperiorKey FROM pmsysDEV.Menu WHERE MenuKey NOT IN (SELECT MenuKey FROM pmsys.Menu);";
$langAe = mysqli_query($DBcon, $langAq);
if ($langAe) {
	echo "Added " . mysqli_affected_rows($DBcon) . "<br>\n";
} else {
	echo "Error on add: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;
}
echo "</p>";
?>