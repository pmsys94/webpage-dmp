<?php
echo "<p>\n";
echo "Try to remove deleted Items:<br>\n";
$langRf = "SELECT ID FROM pmsys.MenuItem WHERE ID NOT IN (SELECT ID from pmsysDEV.MenuItem);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Items " . mysqli_num_rows($langRr) . " to remove.<br>\n";
	while ($langs = mysqli_fetch_array($langRr)) {
		echo "Removing Translation for ID " . $langs["ID"] . "<br>\n";
		$dDIq = "DELETE FROM pmsys.translates WHERE ID = '" . $langs["ID"] . "';";
		$dDIr = mysqli_query($DBcon, $dDIq);
		if($dDIr){
			echo mysqli_affected_rows($DBcon) . " affected Translations.<br>\n";
		} else {
			echo "Failure on del. Translations - " . mysqli_error($DBcon) . "<br>\n";
			$exit = 1;
		}
	}
} else {
	echo "Can not fetch removed Items: " . mysqli_error($DBcon) . "<br>\n";
}
if($exit){
	mysqli_rollback($DBcon);
} else {
	$langRq = "DELETE FROM pmsys.MenuItem WHERE ID NOT IN (SELECT ID FROM pmsysDEV.MenuItem);";
	$langRe = mysqli_query($DBcon, $langRq);
	if($langRe){
		echo "Removed " . mysqli_affected_rows($DBcon) . " Items.<br>\n";
	} else {
		echo "Error in remove: " . mysqli_error($DBcon) . "<br>\n";
		$exit = 1;

	}
}
echo "</p>";
?>