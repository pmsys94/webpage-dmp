<?php
echo "<p>\n";
echo "Try to add missing Items...<br>\n";
$langRf = "SELECT count(ID) AS cnt FROM pmsysDEV.MenuItem WHERE ID NOT IN (SELECT ID FROM pmsys.MenuItem);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	$cnt = mysqli_fetch_array($langRr);
	echo "Fetched Menus to add " . $cnt["cnt"] . "<br>\n";
} else {
	echo "Can not fetch Items to add: " . mysqli_error($DBcon) . "<br>\n";
}
$langAq = "INSERT INTO pmsys.MenuItem(ID, URL, ConnectedMenu, Rank, Submenu) SELECT ID, URL, ConnectedMenu, Rank, Submenu FROM pmsysDEV.MenuItem WHERE ID NOT IN (SELECT ID FROM pmsys.MenuItem);";
$langAe = mysqli_query($DBcon, $langAq);
if ($langAe) {
	echo "Added " . mysqli_affected_rows($DBcon) . "<br>\n";
} else {
	echo "Error on add: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;
}
echo "</p>";
?>