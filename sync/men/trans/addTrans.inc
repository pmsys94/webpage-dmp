<?php
echo "<p>\n";
echo "Try to add missing Items...<br>\n";
$langRf = "SELECT MenuItem, lang FROM pmsysDEV.translates s WHERE NOT EXISTS (SELECT 1 FROM pmsys.translates t WHERE s.MenuItem=t.MenuItem AND s.lang=t.lang);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Translations to add " . mysqli_num_rows($langRr) . "<br>\n";
	echo "Adding:<br>\n";
	while ($toAdd = mysqli_fetch_array($langRr)) {
		echo $toAdd["MenuItem"] . " - " . $toAdd["lang"] . "<br>\n";
	}
} else {
	echo "Can not fetch Translations to add: " . mysqli_error($DBcon) . "<br>\n";
}
$langAq = "INSERT INTO pmsys.translates(MenuItem, EntryText, lang) SELECT MenuItem, EntryText, lang FROM pmsysDEV.translates s WHERE NOT EXISTS (SELECT 1 FROM pmsys.translates t WHERE s.MenuItem=t.MenuItem AND s.lang=t.lang);";
$langAe = mysqli_query($DBcon, $langAq);
if ($langAe) {
	echo "Added " . mysqli_affected_rows($DBcon) . "<br>\n";
} else {
	echo "Error on add: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;
}
echo "</p>";
?>