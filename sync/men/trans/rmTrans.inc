<?php
echo "<p>\n";
echo "Try to remove deleted Translations:<br>\n";
$langRf = "SELECT MenuItem, lang FROM pmsys.translates prod WHERE NOT EXISTS (SELECT 1 from pmsysDEV.translates des WHERE prod.MenuItem=des.MenuItem AND prod.lang=des.lang);";
$langRr = mysqli_query($DBcon, $langRf);
if($langRr){
	echo "Fetched Item Translations " . mysqli_num_rows($langRr) . " to remove.<br>\n";
	while ($langs = mysqli_fetch_array($langRr)) {
		echo "Removing Translation for MenuItem " . $langs["MenuItem"] . " with lang " . $langs["lang"] . "<br>\n";
	}
} else {
	echo "Can not fetch removed Translations: " . mysqli_error($DBcon) . "<br>\n";
}
$langRq = "DELETE d FROM pmsys.translates d WHERE NOT EXISTS (SELECT 1 FROM pmsysDEV.translates des WHERE d.MenuItem=des.MenuItem AND d.lang=des.lang);";
$langRe = mysqli_query($DBcon, $langRq);
if($langRe){
	echo "Removed " . mysqli_affected_rows($DBcon) . " Translations.<br>\n";
} else {
	echo "Error in remove: " . mysqli_error($DBcon) . "<br>\n";
	$exit = 1;

}
echo "</p>";
?>