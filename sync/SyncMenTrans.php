<?php

echo "Script SyncMenTrans Called: " . date("d.m.Y H:i") . "<br>\n";

include "../database/db_write_condat.inc";
$DBcon = mysqli_connect($DBserver, $DBuser, $DBpass, $DBname) OR die(mysqli_connect_error());
$exit = 0;
mysqli_autocommit($DBcon, FALSE);
include 'men/trans/rmTrans.inc';
if($exit){
	mysqli_rollback($DBcon);
	mysqli_close($DBcon);
	die();
}

include 'men/trans/modTrans.inc';
if($exit){
	mysqli_rollback($DBcon);
	mysqli_close($DBcon);
	die();
}

include 'men/trans/addTrans.inc';
if($exit){
	mysqli_rollback($DBcon);
	die();
} else {
	mysqli_commit($DBcon);
}
mysqli_close($DBcon);
?>